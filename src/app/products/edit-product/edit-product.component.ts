import { Component, OnInit ,Output , EventEmitter} from '@angular/core';
import { ProductsService } from './../products.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';


@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  @Output() editProduct:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() editProductPs:EventEmitter<any> = new EventEmitter<any>();

  service:ProductsService;

  updateform = new FormGroup({
    name:new FormControl(),
    price:new FormControl(),

    
  });
  constructor(private route: ActivatedRoute ,service: ProductsService, private router: Router) { 
    this.service = service;
  }
// מעדכן במסד נתונים
  sendData(){
    this.editProduct.emit(this.updateform.value.name);
    console.log(this.updateform.value);

    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putProduct(this.updateform.value, id).subscribe(
        Response=>{
          console.log(Response.json());
          this.editProductPs.emit();
          //חוזר לדף הראשי- מוצרים
          this.router.navigate(['/']);
        }
      )
    })

  }

  product;
  //input מציג את הנתונים בתוך ה 
  ngOnInit() {
     //Login without JWT
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }
  }
  

}