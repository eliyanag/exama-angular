import { Component, OnInit } from '@angular/core';
import { ProductsService } from './../products.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { INVALID } from '@angular/forms/src/model';


@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  
  products;
  productsKeys;
  constructor(private service:ProductsService, private route: ActivatedRoute, private router: Router) {
    this.service = service;
 }
product;
invalid = false;
ngOnInit() {
  this.route.paramMap.subscribe(params=>{
    let id = params.get('name');
    console.log(id);
    this.service.getSearch(id).subscribe(response=>{
      this.products = response.json();
      this.productsKeys = Object.keys(this.products); 
      console.log(this.productsKeys);
      if(this.productsKeys != null)
        this.invalid = true;
      else
        this.invalid = false;
      console.log(this.products);
    })
  })
  //Login without JWT
  var value = localStorage.getItem('auth');
    
  if(value == 'true'){   
    //this.router.navigate(['/']);
  }else{
    this.router.navigate(['/login']);
  }
}

}