import { Injectable } from '@angular/core';
import { HttpParams} from '@angular/common/http';
import {Http, Headers} from '@angular/http';
import { environment } from './../../environments/environment';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class ProductsService {
  http:Http;

  getProductsFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db.list('/products').valueChanges();
  }

  getProducts(){
   return this.http.get(environment.url +'products');
  }

  
  getSearch(name){
    return this.http.get(environment.url + 'search/'  + name);
   }

  getProduct(id){
    return this.http.get(environment.url +'products/'+ id);
 }

 putProduct(data,key){
  let options = {
    headers: new Headers({
      'content-type':'application/x-www-form-urlencoded'
    })
  }
  let params = new HttpParams().append('name',data.name).append('price',data.price);
  return this.http.put(environment.url +'products/'+ key,params.toString(), options);
}

//Login Witout JWT
login(credentials){
  let options = {
     headers:new Headers({
      'content-type':'application/x-www-form-urlencoded'
     })
  }
 let  params = new HttpParams().append('name', credentials.name).append('price',credentials.price);
 return this.http.post(environment.url + 'login', params.toString(),options).map(response=>{ 
   let success = response.json().success;
   if (success == true){
     localStorage.setItem('auth','true');
   }else{
     localStorage.setItem('auth','false');        
   }
});
}


constructor(http:Http, private db:AngularFireDatabase) {
  this.http = http;
 }


}