import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  
  products;
  productsKeys;
  
  //Reactive Form
  searchform = new FormGroup({
  name:new FormControl(),
  
});
search(name){
  this.service.getSearch(name).subscribe(response=>{
      this.products = response.json();      
  })
}

//Q4
updateProduct(id){
      this.service.getProduct(id).subscribe(response=>{
          this.products = response.json();      
     })
   }
   
   sendData(){
    this.route.paramMap.subscribe(params=>{
     let id = params.get('id');
      this.service.getSearch(this.searchform.value.name).subscribe(
        response => {
          console.log(response.json());
          this.router.navigate(['/search-results/' +  this.searchform.value.name]);
        }
      );
    })
  }

  searchProduct(name){
    this.service.getSearch(name).subscribe(response=>{
        this.products = response.json();     
        this.productsKeys = Object.keys(this.products); 
    })
  }
  //Login without JWT router
  constructor(private service:ProductsService, private router: Router ,private route: ActivatedRoute) {
    service.getProducts().subscribe(
      response=>{
        this.products = response.json();
        this.productsKeys = Object.keys(this.products);
    })
  }

  //Login without JWT
  logout(){ 
      localStorage.removeItem('auth');      
      this.router.navigate(['/login']);
  }
  ngOnInit() {
    var value = localStorage.getItem('auth');
  	
    if(value == 'true'){		
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }
  }

}