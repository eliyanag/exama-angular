import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbproductComponent } from './fbproduct.component';

describe('FbproductComponent', () => {
  let component: FbproductComponent;
  let fixture: ComponentFixture<FbproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
