import { Component, OnInit } from '@angular/core';
import { ProductsService } from './../products/products.service';
import { Router } from "@angular/router"; //Login without JWT


@Component({
  selector: 'fbproduct',
  templateUrl: './fbproduct.component.html',
  styleUrls: ['./fbproduct.component.css']
})
export class FbproductComponent implements OnInit {

  products;
  constructor(private service:ProductsService, private router:Router) { }

  ngOnInit() {
    this.service.getProductsFire().subscribe(response=>{
        console.log(response);
        this.products = response;
    });
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }
  }
}