import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProductsService } from './products/products.service';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'; 

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';


import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SearchResultsComponent } from './products/search-results/search-results.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FbproductComponent } from './fbproduct/fbproduct.component';





@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    SearchResultsComponent,
    EditProductComponent,
    SearchResultsComponent,
    EditProductComponent,
    FbproductComponent,
    
    
   
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path: '', component: ProductsComponent},
      {path: 'products', component: ProductsComponent},
      {path: 'login', component: LoginComponent},
      {path: 'search-results/:name', component: SearchResultsComponent},
      {path: 'edit-product/:id', component: EditProductComponent},
      {path: 'fbproduct', component: FbproductComponent},
      {path: 'login', component: LoginComponent},
      {path: '**', component: NotFoundComponent}
       ])
 

  ],
  providers: [
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }