// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/examA/slim/',
  firebase:{
    apiKey: "AIzaSyA5M16kla3gA2F9Z8SOcsNGC5MrYh4gkWw",
    authDomain: "exama-ff85d.firebaseapp.com",
    databaseURL: "https://exama-ff85d.firebaseio.com",
    projectId: "exama-ff85d",
    storageBucket: "exama-ff85d.appspot.com",
    messagingSenderId: "847563027941"
  }
};
